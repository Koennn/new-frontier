var searchData=
[
  ['gameai',['GameAi',['../class_game_ai.html#a0f461b24ed83562f4b21e8e0e08a79ff',1,'GameAi']]],
  ['getactor',['GetActor',['../class_actor_registry.html#a643ea760dcf9c9ed2166b14cc16c7516',1,'ActorRegistry.GetActor()'],['../class_grid_controller.html#a01a7fcf6fb5e09b80254ccbf7a27838a',1,'GridController.GetActor()']]],
  ['getgridcontroller',['GetGridController',['../class_game_state_controller.html#a4e4e4c356f73069100253c936fbe4571',1,'GameStateController']]],
  ['getopponent',['GetOpponent',['../class_game_state_controller.html#a9c398b193b0f09238c73ae8c935b151b',1,'GameStateController']]],
  ['gettilecoords',['GetTileCoords',['../class_grid_controller.html#a6bcee15da4179df46e4b0a8829351fec',1,'GridController']]],
  ['getunit',['GetUnit',['../class_unit_registry.html#ab1c1963577a74a08cd4d69d5ba016643',1,'UnitRegistry']]]
];
