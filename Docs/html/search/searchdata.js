var indexSectionsWithContent =
{
  0: "abcdefghiklmnoprstu",
  1: "abegikmpstu",
  2: "abegikmpstu",
  3: "cefghilmostu",
  4: "acdeilmnprstu",
  5: "ag",
  6: "abeghimp",
  7: "bms"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "enums",
  6: "enumvalues",
  7: "properties"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Enumerations",
  6: "Enumerator",
  7: "Properties"
};

