var searchData=
[
  ['level',['Level',['../class_player.html#ad89cb407e6df206d7a21a714006571c9',1,'Player']]],
  ['loadedstate',['LoadedState',['../class_battle_loader.html#a502bbddcdc898e4bb0e6970501b8131e',1,'BattleLoader']]],
  ['loadoperation',['LoadOperation',['../class_game_state.html#ad42e6f67f9f2d740b5e0eb3854834802',1,'GameState']]],
  ['local',['Local',['../class_unit_place_event.html#a19d1d0058bcd360e05e5c9916dda9c39',1,'UnitPlaceEvent']]],
  ['localgrid',['LocalGrid',['../class_game_state.html#aa4176315a5cabeeb0daa3553f3ca9843',1,'GameState']]],
  ['localplayer',['LocalPlayer',['../class_game_state.html#ac85e9b25c3f402d1e6ea8fad0d2abdec',1,'GameState.LocalPlayer()'],['../class_test_battle_info.html#a72d59ae64953cf528a5b3585ea7be4b6',1,'TestBattleInfo.LocalPlayer()']]],
  ['localturn',['LocalTurn',['../class_game_state.html#a01e28528f4abb0576c7f27a58fd628ab',1,'GameState']]]
];
