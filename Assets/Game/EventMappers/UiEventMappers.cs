﻿using UnityEngine;

public class UiEventMappers : IEventListener {
    private Actor _previousSelect;
    private Ability _selecedAbility;

    [EventListener(typeof(MouseButtonDownEvent))]
    public void OnMouseButtonDown(MouseButtonDownEvent eventInfo) {
        if (GameObject.Find("UI").transform.Find("UnitPlace") == null) {
            return;
        }

        var ray = Camera.main.ScreenPointToRay(new Vector3(eventInfo.MousePos.x, eventInfo.MousePos.y));
        RaycastHit hitInfo;
        if (!Physics.Raycast(ray, out hitInfo, 30)) {
            return;
        }

        if (!hitInfo.transform.gameObject.CompareTag("Tile")) {
            return;
        }

        if (!GameObject.Find("Control").GetComponent<Player>().Turn) {
            return;
        }

        var targetHelper = GameObject.Find("Control").GetComponent<TargetHelper>();
        if (_selecedAbility != null) {
            if (targetHelper.IsPossible) {
                Debug.Log("Executing ability " + _selecedAbility.Properties.Name);
                EventController.Instance.CallEvent(new UnitAbilityExecuteEvent(true, _selecedAbility, _previousSelect, GameObject.Find("Remote Grid").GetComponent<GridController>()));
                EventController.Instance.CallEvent(new AbilityCancelledEvent(_selecedAbility, GameObject.Find("Local Grid").GetComponent<GridController>()));
            }
        }

        if (_previousSelect != null) {
            if (targetHelper.IsPossible) {
                EventController.Instance.CallEvent(new UnitDeselectEvent(_previousSelect));
                _previousSelect = null;
                _selecedAbility = null;
                return;
            }
        }

        var gridController = GameObject.Find("Local Grid").GetComponent<GridController>();
        var tilePos = gridController.GetTileCoords(hitInfo.transform.gameObject);
        if (hitInfo.transform.childCount == 0) {
            return;
        }

        if (tilePos.x < 0) {
            return;
        }
        var actor = gridController.GetActor((int) tilePos.x, (int) tilePos.y).GetComponent<Actor>();
        _previousSelect = actor;
        EventController.Instance.CallEvent(new UnitSelectEvent(actor));
    }

    [EventListener(typeof(AbilityButtonPressedEvent))]
    public void OnAbilityButtonPressed(AbilityButtonPressedEvent eventInfo) {
        if (eventInfo.Ability.OnCooldown()) {
            return;
        }

        if (_selecedAbility != null) {
            EventController.Instance.CallEvent(new AbilityCancelledEvent(_selecedAbility, GameObject.Find("Local Grid").GetComponent<GridController>()));
        }

        Debug.Log("Ability button pressed: " + eventInfo.Ability.Properties.Name);
        EventController.Instance.CallEvent(new UnitAbilityUseEvent(eventInfo.Actor, eventInfo.Ability));
        _selecedAbility = eventInfo.Ability;
    }

    [EventListener(typeof(KeyPressEvent))]
    public void OnKeyPress(KeyPressEvent eventInfo) {
        if (Input.GetKeyDown(KeyCode.Escape)) {
            GameObject.Find("Control").GetComponent<TargetHelper>().StopTargeting();
        }
    }
}