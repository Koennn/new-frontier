﻿using UnityEngine;

[AddComponentMenu("Turn System/Player")]
public class Player : MonoBehaviour, IEventListener {
    public string Username;
    public int Level;
    public bool IsAi;
    public GridController Grid;
    public bool Turn;

    private void FixedUpdate() { }

    [EventListener(typeof(EndTurnEvent))]
    public void OnEndTurn(EndTurnEvent eventInfo) {
        Turn = eventInfo.NextTurn == this;
    }

    public void EndTurn() {
        var turnController = GameObject.Find("Control").GetComponent<TurnController>();
        EventController.Instance.CallEvent(new EndTurnEvent(true, this, turnController.Player1 == this ? turnController.Player2 : turnController.Player1));
    }
}