﻿using System.Collections;
using UnityEngine;

[AddComponentMenu("Turn System/AI")]
public class AI : MonoBehaviour, IEventListener {
    public Player Player;
    private bool _started;

    private void FixedUpdate() {
        if (!Player.Turn || _started) {
            return;
        }

        _started = true;
        StartCoroutine(Attack());
    }

    private IEnumerator Attack() {
        yield return new WaitForSeconds(2);
        var enemyGrid = GameObject.Find("Local Grid").GetComponent<GridController>();
        Actor attacker = null;
        Actor target = null;
        Ability ability = null;

        for (var x = 1; x < 5; x++) {
            for (var y = 1; y < 4; y++) {
                if (!Player.Grid.HasActor(x, y)) {
                    continue;
                }

                attacker = Player.Grid.GetActor(x, y).GetComponent<Actor>();
                break;
            }
        }

        for (var x = 1; x < 5; x++) {
            for (var y = 1; y < 4; y++) {
                if (!enemyGrid.HasActor(x, y)) {
                    continue;
                }

                target = enemyGrid.GetActor(x, y).GetComponent<Actor>();
                break;
            }
        }

        if (attacker != null) {
            foreach (var a in attacker.Abilities) {
                Debug.Log("[AI] Checking ability \'" + a.Properties.Name + "\'");
                Debug.Log("[AI] OnCooldown " + a.OnCooldown());
                if (a.OnCooldown()) {
                    continue;
                }
                ability = a;
                break;
            }
        }

        if (attacker == null || target == null || ability == null) {
            _started = false;
            Player.EndTurn();
        } else {
            Debug.Log("[AI] Selected ability \'" + ability.Properties.Name + "\'");
            Debug.Log("Executing ability " + ability.Properties.Name);

            var targetHelper = GameObject.Find("Control").GetComponent<TargetHelper>();
            ability.Execute(enemyGrid, targetHelper.AITarget(ability.Range, ability.Pattern, enemyGrid, target.transform.parent.gameObject));
            _started = false;
            Player.EndTurn();
        }
    }

    [EventListener(typeof(EndTurnEvent))]
    public void OnEndTurn(EndTurnEvent eventInfo) {
        _started = false;
    }
}