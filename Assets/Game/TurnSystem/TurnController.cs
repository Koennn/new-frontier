﻿using UnityEngine;

[AddComponentMenu("Turn System/Turn Controller")]
public class TurnController : MonoBehaviour, IEventListener {
    public Player Player1;
    public Player Player2;
    public int MaxTurnTime;

    [HideInInspector] public int Turn;
    [HideInInspector] public Player CurrentTurn;
    [HideInInspector] public int TurnTime;

    private bool _started;
    private float _nextTime;

    private void FixedUpdate() {
        if (!_started) {
            return;
        }

        if (CurrentTurn == null) {
            CurrentTurn = Player1;
            EventController.Instance.CallEvent(new EndTurnEvent(true, null, Player1));
        } else {
            if (!(Time.time > _nextTime)) {
                return;
            }
            _nextTime += 1;
            TurnTime++;

            if (TurnTime != MaxTurnTime || Player2.IsAi) {
                return;
            }
            EventController.Instance.CallEvent(new EndTurnEvent(true, CurrentTurn, CurrentTurn == Player1 ? Player2 : Player1));
        }
    }

    public void StartGame() {
        if (_started) {
            Debug.LogWarning("Attempting to start game while already started.");
            return;
        }

        _started = true;
    }

    [EventListener(typeof(EndTurnEvent))]
    public void OnEndTurn(EndTurnEvent eventInfo) {
        Debug.Log("Setting current turn to \'" + eventInfo.NextTurn.Username + "\'");
        CurrentTurn = eventInfo.NextTurn;
    }
}