﻿public enum HitChance {
    VeryLow,
    Low,
    MediumLow,
    Medium,
    MediumHigh,
    High,
    VeryHigh
}