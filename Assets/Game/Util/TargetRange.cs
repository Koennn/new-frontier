﻿using System;

[Serializable]
public class TargetRange {
    public int Min, Max;
    public TargetType Type;
}