﻿using UnityEngine;
using UnityEngine.UI;

public class TitleManager : MonoBehaviour {
    public static TitleManager Instance;

    public string CurrentText;
    public Color CurrentColor;
    public int TimeLeft;

    private float _nextTime;

    public void DisplayTitle(string text, Color color, int displayTime) {
        CurrentText = text;
        CurrentColor = color;
        TimeLeft = displayTime;
    }

    private void Start() {
        Instance = this;
    }

    private void FixedUpdate() {
        if (!(Time.time >= _nextTime)) {
            return;
        }
        _nextTime += 1;
        TimeLeft--;

        if (TimeLeft > 0) {
            GetComponent<Text>().text = CurrentText;
            GetComponent<Text>().color = CurrentColor;
        } else {
            GetComponent<Text>().text = "";
        }
    }
}