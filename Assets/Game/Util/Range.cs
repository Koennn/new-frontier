﻿using System;

[Serializable]
public class Range {
    public int Min, Max;
}