﻿using System;
using UnityEngine;
using Random = UnityEngine.Random;

[Serializable]
public class Pattern {
    public int Depth, Width;
    public PatternType Type;
    public bool LockFree;

    public Vector2[] GetTargets(Vector2 target) {
        var targets = new Vector2[Width * Depth];
        var index = 0;
        var offset = 1;
        switch (Type) {
            case PatternType.Wave:
                for (var i = 0; i < Depth; i++) {
                    for (var j = 1; j < Width + 1; j++) {
                        if (j == 1) {
                            targets[index++] = new Vector2(target.x, target.y + i);
                        } else if (j % 2 == 0) {
                            targets[index++] = new Vector2(target.x - offset, target.y + i);
                        } else {
                            targets[index++] = new Vector2(target.x + offset, target.y + i);
                            offset++;
                        }
                    }
                }
                return targets;
            case PatternType.Penetration:
                return targets;
            case PatternType.Scatter:
                targets[0] = new Vector2((int) Random.Range(target.x, target.x + Width), (int) Random.Range(target.y, target.y + Depth));
                return targets;
            default:
                throw new ArgumentOutOfRangeException();
        }
    }
}