﻿using System.Collections.Generic;
using UnityEngine;

[AddComponentMenu("Misc/Target Helper")]
public class TargetHelper : MonoBehaviour, IEventListener {
    public Vector2[] Targets { get; private set; }
    public bool IsPossible { get; private set; }
    public Color TargetColor;

    private bool _targeting;
    //TODO: Implement ability range
    private TargetRange _range;
    private Pattern _pattern;
    private GridController _grid;
    private Vector2[] _lastColored;
    private bool _allowTargeting;

    public void StartTargeting(TargetRange range, Pattern pattern, GridController targetable) {
        if (_targeting) {
            StopTargeting();
        }

        _targeting = true;
        _allowTargeting = true;
        _range = range;
        _pattern = pattern;
        _grid = targetable;
    }

    public void StopTargeting() {
        if (_lastColored == null) {
            _targeting = false;
            IsPossible = false;
            return;
        }

        foreach (var tile in _lastColored) {
            _grid.ResetTileColor(tile);
        }
        _lastColored = null;
        _targeting = false;
        IsPossible = false;
        _allowTargeting = false;
    }

    public Vector2[] AITarget(TargetRange range, Pattern pattern, GridController targetable, GameObject tile) {
        if (_targeting) {
            StopTargeting();
        }

        _allowTargeting = false;
        _range = range;
        _pattern = pattern.Depth != 0 && pattern.Width != 0 ? pattern : null;
        _grid = targetable;

        Debug.Log("Calculating targets on '" + tile + "'");
        UpdateTargets(tile);
        StopTargeting();
        return Targets;
    }

    private void Update() {
        if (!_targeting || !_allowTargeting) {
            return;
        }

        var ray = Camera.main.ScreenPointToRay(new Vector3(Input.mousePosition.x, Input.mousePosition.y));
        RaycastHit hitInfo;
        if (!Physics.Raycast(ray, out hitInfo, 30)) {
            return;
        }

        var tile = hitInfo.transform.gameObject;
        if (!tile.CompareTag("Tile")) {
            return;
        }

        UpdateTargets(tile);
    }

    private void UpdateTargets(GameObject tile) {
        if (_pattern == null) {
            var tileCoords = _grid.GetTileCoords(tile);
            if (tileCoords.x == -1) {
                return;
            }

            _grid.ColorTile(tileCoords, TargetColor);
            IsPossible = _grid.HasActor((int) tileCoords.x, (int) tileCoords.y);
            Targets = new[] {tileCoords};
            if (_lastColored != null && !_lastColored[0].Equals(tileCoords) && _lastColored[0].x > 0) {
                _grid.ResetTileColor(_lastColored[0]);
            }
            _lastColored = new[] {tileCoords};
        } else {
            var tileCoords = _grid.GetTileCoords(tile);
            if (tileCoords.x == -1) {
                return;
            }

            var possibleTargets = new List<Vector2>();
            Targets = _pattern.GetTargets(tileCoords);
            foreach (var target in Targets) {
                if (!_grid.IsWithinBounds(target)) {
                    continue;
                }

                _grid.ColorTile(target, TargetColor);
                possibleTargets.Add(target);
            }

            IsPossible = _pattern.LockFree ? possibleTargets.Count > 0 : _grid.HasActor((int) tileCoords.x, (int) tileCoords.y);

            if (_lastColored != null && !_lastColored[0].Equals(tileCoords) && _lastColored[0].x > 0) {
                foreach (var lastColored in _lastColored) {
                    if (_grid.IsWithinBounds(lastColored)) {
                        _grid.ResetTileColor(lastColored);
                    }
                }
            }
            _lastColored = new Vector2[Targets.Length];
            possibleTargets.CopyTo(_lastColored, 0);
        }
    }
}