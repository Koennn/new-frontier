﻿public enum PatternType {
    Wave,
    Penetration,
    Scatter
}