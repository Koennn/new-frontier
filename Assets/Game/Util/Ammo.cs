﻿using UnityEngine;

[AddComponentMenu("Actor/Ammo")]
public class Ammo : MonoBehaviour {
    public int MaxAmmo;
    public int Reload;
    [HideInInspector] public int CurrentAmmo;
}