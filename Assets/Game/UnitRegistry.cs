﻿using System.Linq;
using UnityEngine;

public class UnitRegistry : MonoBehaviour {
    public GameObject[] Actors;

    public GameObject GetUnit(string unitName) {
        return (from unit in Actors let actor = unit.GetComponent<Actor>() where actor != null where actor.Name == unitName select unit).FirstOrDefault();
    }
}