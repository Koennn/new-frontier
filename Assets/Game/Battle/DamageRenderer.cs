﻿using UnityEngine;

public class DamageRenderer : MonoBehaviour {
    public GameObject HpParticlePrefab;

    public void RenderDamage(Actor actor, int amount) {
        var particle = Instantiate(HpParticlePrefab);
        particle.transform.SetParent(actor.gameObject.transform);
        particle.transform.localPosition = new Vector3(0, 2, 0);

        var label = particle.transform.GetChild(0).gameObject.GetComponent<TextMesh>();
        label.text = (amount > 0 ? "-" : "+") + amount;
        label.color = Color.white;

        particle.GetComponent<AlwaysFace>().Target = Camera.main.gameObject;
    }
}