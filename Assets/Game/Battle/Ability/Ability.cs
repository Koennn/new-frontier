﻿using UnityEngine;

public abstract class Ability : MonoBehaviour {
    public AbilityProperties Properties;
    public Pattern Pattern;
    public TargetRange Range;

    public abstract void Load();

    public abstract void Target(GridController targetGrid, TargetHelper targetHelper);

    public abstract void StopTargeting(GridController targetGrid, TargetHelper targetHelper);

    public abstract void Execute(GridController targetGrid, Vector2[] targets);

    public bool OnCooldown() {
        return Properties.CurrentCooldown > 0;
    }
}