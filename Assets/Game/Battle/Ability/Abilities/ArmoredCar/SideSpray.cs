﻿using UnityEngine;

[AddComponentMenu("Abilites/Armored Car/Side Spray")]
public class SideSpray : Ability {
    [Space] public HitChance HitChance;
    public int AttackCount;
    public int AmmoUse;
    public Range Damage;

    public override void Load() { }

    public override void Target(GridController targetGrid, TargetHelper targetHelper) {
        targetHelper.StartTargeting(Range, Pattern, targetGrid);
    }

    public override void StopTargeting(GridController targetGrid, TargetHelper targetHelper) {
        targetHelper.StopTargeting();
    }

    public override void Execute(GridController targetGrid, Vector2[] targets) {
        foreach (var target in targets) {
            targetGrid.DamageTile(target, new Damage(Random.Range(Damage.Min, Damage.Max), global::Damage.DamageType.Normal));
        }
    }
}