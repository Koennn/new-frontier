﻿using UnityEngine;

[AddComponentMenu("Abilites/Armored Car/Direct Assault")]
public class DirectAssault : Ability {
    [Space] public HitChance HitChance;
    public int AttackCount;
    public int AmmoUse;
    public Range Damage;

    public override void Load() { }

    public override void Target(GridController grid, TargetHelper targetHelper) {
        targetHelper.StartTargeting(Range, null, grid);
    }

    public override void StopTargeting(GridController grid, TargetHelper targetHelper) {
        targetHelper.StopTargeting();
    }

    public override void Execute(GridController grid, Vector2[] targets) {
        foreach (var target in targets) {
            grid.DamageTile(target, new Damage(Random.Range(Damage.Min, Damage.Max), global::Damage.DamageType.Normal));
        }
    }
}