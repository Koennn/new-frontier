﻿using System;
using UnityEngine;

[Serializable]
public class AbilityProperties {
    public string Name;
    public Sprite Icon;
    public int Cooldown;
    [HideInInspector] public int CurrentCooldown;
}