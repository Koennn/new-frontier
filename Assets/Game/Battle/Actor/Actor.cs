﻿using System;
using System.Collections;
using UnityEngine;

[AddComponentMenu("Actor/Base Actor")]
public class Actor : MonoBehaviour, IEventListener {
    /// <summary>
    /// The id for the next actor to be constructed.
    /// </summary>
    private static int _nextId;

    /// <summary>
    /// The type of the actor.
    /// </summary>
    public ActorType Type;

    /// <summary>
    /// The name of the actor (Used to look up in registry).
    /// </summary>
    public string Name;

    public Health Health;

    public Skin[] Skins;

    public string SelectedSkin;

    /// <summary>
    /// Unique identifier of the actor.
    /// </summary>
    [HideInInspector] public int Id;

    /// <summary>
    /// Position of the actor on the grid.
    /// </summary>
    [HideInInspector] public Vector2 Position;

    [HideInInspector] public Ability[] Abilities;

    /// <summary>
    /// Initialize the actor.
    /// </summary>
    public void Init() {
        EventController.Instance.RegisterListener(this);
        var abilities = GetComponents(typeof(Ability));
        Abilities = new Ability[abilities.Length];
        for (var i = 0; i < abilities.Length; i++) {
            if (abilities[i] == null) {
                continue;
            }
            Abilities[i] = (Ability) abilities[i];
            Abilities[i].Load();
        }

        Id = _nextId++;

        Skin selectedSkin = null;
        foreach (var skin in Skins) {
            if (string.Equals(skin.Name, SelectedSkin, StringComparison.CurrentCultureIgnoreCase)) {
                selectedSkin = skin;
            }
        }
        if (selectedSkin != null) {
            foreach (Transform child in transform) {
                var mesh = child.gameObject.GetComponent<MeshRenderer>();
                if (mesh == null) {
                    continue;
                }
                foreach (var material in mesh.materials) {
                    var colorName = material.name.Replace(" (Instance)", "");
                    var color = selectedSkin.GetColor(colorName);
                    if (color != null) {
                        material.color = color.Color;
                    }
                }
            }
        }

        Debug.Log("Initialized actor \'" + Name + "@" + Position + "\'");
    }

    [EventListener(typeof(UnitDamageReceiveEvent))]
    public void OnUnitDamageReceive(UnitDamageReceiveEvent eventInfo) {
        if (eventInfo.Actor != this) {
            return;
        }

        Debug.Log("Rendering damage...");
        GameObject.Find("Control").GetComponent<DamageRenderer>().RenderDamage(this, eventInfo.Damage.Amount);
    }
}