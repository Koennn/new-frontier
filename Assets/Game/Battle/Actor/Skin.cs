﻿using System;
using System.Linq;

[Serializable]
public class Skin {
    public string Name;
    public SkinColor[] Colors;

    public SkinColor GetColor(string name) {
        return Colors.FirstOrDefault(color => color.Name == name);
    }
}