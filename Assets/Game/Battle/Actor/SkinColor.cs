﻿using System;
using UnityEngine;

[Serializable]
public class SkinColor {
    public string Name;
    public Color Color;
}