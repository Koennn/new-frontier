﻿#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;

[CustomPropertyDrawer(typeof(SkinColor))]
public class SkinColorDrawer : PropertyDrawer {
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
        EditorGUI.BeginProperty(position, label, property);
        position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);

        var indent = EditorGUI.indentLevel;
        EditorGUI.indentLevel = 0;

        var amountRect = new Rect(position.x, position.y, 100, position.height);
        var nameRect = new Rect(position.x + 105, position.y, position.width - 105, position.height);

        EditorGUI.PropertyField(amountRect, property.FindPropertyRelative("Name"), GUIContent.none);
        EditorGUI.PropertyField(nameRect, property.FindPropertyRelative("Color"), GUIContent.none);

        EditorGUI.indentLevel = indent;
        EditorGUI.EndProperty();
    }
}

#endif