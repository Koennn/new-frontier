﻿public enum ActorType {
    Infantry,
    GroundVehicle,
    AirVehicle
}