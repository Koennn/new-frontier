﻿using System;
using UnityEngine;

[Serializable]
public class Health {
    public int MaxHealth;
    [HideInInspector] public int CurrentHealth;
    public int MaxArmor;
    [HideInInspector] public int CurrentArmor;

    public void Damage(Damage damage) { }
}