﻿using UnityEngine;

public class BattlePhaseListeners : IEventListener {
    [EventListener(typeof(UnitAbilityUseEvent))]
    public void OnUnitAbilityUse(UnitAbilityUseEvent eventInfo) {
        if (eventInfo.Ability == null) {
            return;
        }

        Debug.Log("Ability use: " + eventInfo.Ability.Properties.Name);
        var targetHelper = GameObject.Find("Control").GetComponent<TargetHelper>();
        eventInfo.Ability.Target(GameObject.Find("Remote Grid").GetComponent<GridController>(), targetHelper);
    }

    [EventListener(typeof(AbilityCancelledEvent))]
    public void OnAbilityCancel(AbilityCancelledEvent eventInfo) {
        if (eventInfo.Ability == null) {
            return;
        }

        var targetHelper = GameObject.Find("Control").GetComponent<TargetHelper>();
        eventInfo.Ability.StopTargeting(eventInfo.Grid, targetHelper);
    }

    [EventListener(typeof(UnitAbilityExecuteEvent))]
    public void OnUnitAbilityExecute(UnitAbilityExecuteEvent eventInfo) {
        if (eventInfo.Ability == null) {
            return;
        }

        if (eventInfo.Ability.OnCooldown()) {
            return;
        }

        EventController.Instance.CallEvent(new UnitDeselectEvent(eventInfo.Actor));

        if (eventInfo.FromLocal) {
            var targetHelper = GameObject.Find("Control").GetComponent<TargetHelper>();
            eventInfo.Ability.Execute(eventInfo.Grid, targetHelper.Targets);
        } else {
            //TODO: Ability execute from remote (multiplayer only).
        }

        var turnController = GameObject.Find("Control").GetComponent<TurnController>();
        EventController.Instance.CallEvent(new EndTurnEvent(true, turnController.CurrentTurn, turnController.Player1 == turnController.CurrentTurn ? turnController.Player2 : turnController.Player1));
    }

    [EventListener(typeof(BattlePhaseStartEvent))]
    public void OnBattlePhaseStart(BattlePhaseStartEvent eventInfo) {
        GameObject.Find("Control").GetComponent<TurnController>().StartGame();
    }

    [EventListener(typeof(EndTurnEvent))]
    public void OnEndTurn(EndTurnEvent eventInfo) {
        TitleManager.Instance.DisplayTitle(eventInfo.NextTurn.Username + "\'s turn!", Color.black, 4);
    }
}