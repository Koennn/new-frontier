﻿using System.Collections;
using UnityEngine;

public class PrepPhaseListeners : IEventListener {
    private GameObject _dragging;

    [EventListener(typeof(StartUnitDragEvent))]
    public void OnStartUnitDrag(StartUnitDragEvent eventInfo) {
        _dragging = GameObject.Find("Control").GetComponent<UnitRegistry>().GetUnit(eventInfo.UnitName);
        if (_dragging == null) {
            return;
        }

        GameObject.Find("UI").transform.Find("UnitPlace").gameObject.SetActive(false);
        _dragging = Object.Instantiate(_dragging);
        _dragging.transform.position = new Vector3(0, -10, 0);
    }

    [EventListener(typeof(UnitDragEvent))]
    public void OnUnitDrag(UnitDragEvent eventInfo) {
        if (_dragging == null) {
            return;
        }

        var ray = Camera.main.ScreenPointToRay(new Vector3(eventInfo.MousePos.x, eventInfo.MousePos.y));
        RaycastHit hitInfo;
        if (!Physics.Raycast(ray, out hitInfo, 30)) {
            return;
        }

        var tile = hitInfo.transform.gameObject;
        if (!tile.tag.Equals("Tile")) {
            _dragging.transform.SetParent(null);
            _dragging.transform.position = new Vector3(0, -10, 0);
            return;
        }

        if (tile.transform.parent.gameObject.name != "Local Grid") {
            return;
        }

        var gridController = GameObject.Find("Local Grid").GetComponent<GridController>();
        var tileCoords = gridController.GetTileCoords(tile);
        gridController.SetActor(_dragging, (int) tileCoords.x, (int) tileCoords.y, false);
    }

    [EventListener(typeof(StopUnitDragEvent))]
    public void OnStopUnitDrag(StopUnitDragEvent eventInfo) {
        if (_dragging == null) {
            return;
        }

        Object.Destroy(_dragging);
        if (_dragging.transform.parent != null) {
            var parent = _dragging.transform.parent.gameObject;
            var gridController = GameObject.Find("Local Grid").GetComponent<GridController>();
            var actor = _dragging.GetComponent<Actor>();
            GameObject.Find("Control").GetComponent<UnitRegistry>().StartCoroutine(CallUnitPlace(gridController, actor, parent, true));
        }

        GameObject.Find("UI").transform.Find("UnitPlace").gameObject.SetActive(true);
        _dragging = null;
    }

    [EventListener(typeof(UnitPlaceEvent))]
    public void OnUnitPlace(UnitPlaceEvent eventInfo) {
        var actor = GameObject.Find("Control").GetComponent<UnitRegistry>().GetUnit(eventInfo.UnitName);
        if (eventInfo.Local) {
            var grid = GameObject.Find("Local Grid").GetComponent<GridController>();
            grid.SetActor(Object.Instantiate(actor), (int) eventInfo.Position.x, (int) eventInfo.Position.y, false);
            grid.GetActor((int) eventInfo.Position.x, (int) eventInfo.Position.y).GetComponent<Actor>().Init();
        } else {
            var grid = GameObject.Find("Remote Grid").GetComponent<GridController>();
            grid.SetActor(Object.Instantiate(actor), (int) eventInfo.Position.x, (int) eventInfo.Position.y, true);
            grid.GetActor((int) eventInfo.Position.x, (int) eventInfo.Position.y).GetComponent<Actor>().Init();
        }
    }

    public static IEnumerator CallUnitPlace(GridController controller, Actor actor, GameObject tile, bool local) {
        yield return new WaitUntil(() => tile.transform.childCount == 0);
        EventController.Instance.CallEvent(new UnitPlaceEvent(actor.Name, controller.GetTileCoords(tile), local));
    }
}