﻿public class Damage {
    public int Amount;
    public DamageType Type;

    public Damage(int amount, DamageType type) {
        Amount = amount;
        Type = type;
    }

    public enum DamageType {
        Normal
    }
}