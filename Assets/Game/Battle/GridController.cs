﻿using System;
using System.Collections;
using UnityEngine;

[AddComponentMenu("Engine/Grid Controller")]
public class GridController : MonoBehaviour {
    /// <summary>
    /// Temporary test actor object.
    /// </summary>
    public GameObject TmpActor;

    public Material TileMaterial;

    public int Width, Height;

    /// <summary>
    /// All tiles in the grid.
    /// </summary>
    private GameObject[] _tiles;

    /// <summary>
    /// Initializes the GridController and loads all tiles.
    /// </summary>
    private void Start() {
        _tiles = new GameObject[transform.childCount];

        var index = 0;
        foreach (var child in transform) {
            _tiles[index] = ((Transform) child).gameObject;
            _tiles[index].GetComponent<MeshRenderer>().material = new Material(TileMaterial);
            index++;
        }

        if (name != "Remote Grid") {
            return;
        }

        var parent = GetTile(3, 3);
        var actor = GameObject.Find("Control").GetComponent<UnitRegistry>().GetUnit("Armored Car").GetComponent<Actor>();
        StartCoroutine(PrepPhaseListeners.CallUnitPlace(this, actor, parent, name == "Local Grid"));
    }

    /// <summary>
    /// Get a tile at specific coordinates.
    /// </summary>
    /// <param name="x">x coordinate of the tile</param>
    /// <param name="y">y coordinate of the tile</param>
    /// <returns>The tile at the coordinates</returns>
    private GameObject GetTile(int x, int y) {
        return IsWithinBounds(new Vector2(x, y)) ? _tiles[y * 5 - 5 + x - 1] : null;
    }

    /// <summary>
    /// Check if the specified coordinates are within the grid bounds
    /// </summary>
    /// <param name="coords">Coordinates to check</param>
    /// <returns>true if the coordinates are withing the bounds</returns>
    public bool IsWithinBounds(Vector2 coords) {
        try {
            return coords.x <= Width && coords.x > 0 && coords.y <= Height && coords.y > 0;
        } catch (IndexOutOfRangeException) {
            return false;
        }
    }

    /// <summary>
    /// Get the coodinates of a specified tile.
    /// </summary>
    /// <param name="tile">Tile to get the coordinates for</param>
    /// <returns>Coordinates of the tile</returns>
    public Vector2 GetTileCoords(GameObject tile) {
        var index = 0;
        foreach (var currTile in _tiles) {
            if (currTile == tile) {
                var y = index / 5 + 1;
                return new Vector2(index + 1 - (y * 5 - 5), y);
            }
            index++;
        }
        return new Vector2(-1, -1);
    }

    /// <summary>
    /// Check if the tile at specific coordinates has an actor child.
    /// </summary>
    /// <param name="x">x coordinate of the tile</param>
    /// <param name="y">y coordinate of the tile</param>
    /// <returns></returns>
    public bool HasActor(int x, int y) {
        return IsWithinBounds(new Vector2(x, y)) && GetTile(x, y).transform.childCount > 0;
    }

    /// <summary>
    /// Set the actor of a tile at specific coordinates.
    /// </summary>
    /// <param name="actor">Actor to set</param>
    /// <param name="x">x coordinate of the tile</param>
    /// <param name="y">y coordinate of the tile</param>
    /// <param name="rotate">y rotation of the actor</param>
    /// <returns></returns>
    public bool SetActor(GameObject actor, int x, int y, bool rotate) {
        if (HasActor(x, y)) {
            return false;
        }

        if (actor.GetComponent(typeof(Actor)) == null) {
            return false;
        }

        var actorTransform = actor.transform;
        actorTransform.SetParent(GetTile(x, y).transform);
        actorTransform.localPosition = new Vector3(0, 0, 0);
        ((Actor) actor.GetComponent(typeof(Actor))).Position = new Vector2(x, y);
        //actorTransform.Rotate(new Vector3(0, 0, 1), 180);
        if (rotate) {
            actorTransform.Rotate(new Vector3(0, 1, 0), 180);
            actorTransform.localPosition = new Vector3(actorTransform.localPosition.x + 2.0F, 0, actorTransform.localPosition.z);
        } else {
            actorTransform.localPosition = new Vector3(actorTransform.localPosition.x - 1.0F, 0, actorTransform.localPosition.z);
        }
        return true;
    }

    /// <summary>
    /// Get the actor of a tile at specific coordinates.
    /// </summary>
    /// <param name="x">x coordinate of the tile</param>
    /// <param name="y">y coordinate of the tile</param>
    /// <returns>The actor of the tile at the coordinates</returns>
    public GameObject GetActor(int x, int y) {
        return HasActor(x, y) ? GetTile(x, y).transform.GetChild(0).gameObject : null;
    }

    /// <summary>
    /// Renders the tile in a certain color red depending on the damage dealt.
    /// </summary>
    /// <param name="tile">Tile to render the damage on</param>
    /// <param name="damage">Amount of damage to render</param>
    public void DamageTile(Vector2 tile, Damage damage) {
        StartCoroutine(ColorTileSynced(Color.red, GetTile((int) tile.x, (int) tile.y)));
        StartCoroutine(DelayedUncolorTile(GetTile((int) tile.x, (int) tile.y)));

        if (!HasActor((int) tile.x, (int) tile.y)) {
            return;
        }

        var actor = GetActor((int) tile.x, (int) tile.y).GetComponent<Actor>();
        EventController.Instance.CallEvent(new UnitDamageReceiveEvent(actor, new Damage(damage.Amount, damage.Type)));
    }

    private IEnumerator ColorTileSynced(Color color, GameObject tile) {
        yield return new WaitForEndOfFrame();
        ColorTile(GetTileCoords(tile), color);
    }

    private IEnumerator DelayedUncolorTile(GameObject tile) {
        yield return new WaitForSeconds(1);
        ResetTileColor(GetTileCoords(tile));
    }

    /// <summary>
    /// Color the tile a the specified coordinates in a specified color.
    /// </summary>
    /// <param name="tile">Tile to color</param>
    /// <param name="color">Color for the tile</param>
    public void ColorTile(Vector2 tile, Color color) {
        var tileObject = GetTile((int) tile.x, (int) tile.y);
        if (tileObject == null) {
            return;
        }
        var material = tileObject.GetComponent<MeshRenderer>().material;
        StandardShaderUtils.ChangeRenderMode(material, StandardShaderUtils.BlendMode.Opaque);
        material.SetColor("_EmissionColor", color);
    }

    /// <summary>
    /// Reset the color of a tile at the specified coordinates.
    /// </summary>
    /// <param name="tile">Coordinates of the tile to reset</param>
    public void ResetTileColor(Vector2 tile) {
        var tileObject = GetTile((int) tile.x, (int) tile.y);
        if (tileObject == null) {
            return;
        }
        var material = tileObject.GetComponent<MeshRenderer>().material;
        StandardShaderUtils.ChangeRenderMode(material, StandardShaderUtils.BlendMode.Fade);
        material.SetColor("_EmissionColor", Color.black);
    }

    public GameObject[] GetTiles() {
        return (GameObject[]) _tiles.Clone();
    }
}