public class SyncEvent : IEvent {
    public bool FromLocal;

    public SyncEvent(bool fromLocal) {
        FromLocal = fromLocal;
    }
}