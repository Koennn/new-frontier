﻿using System;

public class EventListener : Attribute {
    public Type EventType;

    public EventListener(Type eventType) {
        EventType = eventType;
    }
}