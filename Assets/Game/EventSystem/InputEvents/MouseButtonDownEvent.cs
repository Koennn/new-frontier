﻿using UnityEngine;

public class MouseButtonDownEvent : IEvent {
    public int Button { get; }
    public Vector3 MousePos { get; }

    public MouseButtonDownEvent(Vector3 mousePos, int button) {
        MousePos = mousePos;
        Button = button;
    }
}