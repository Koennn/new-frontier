﻿public class MouseScrollEvent : IEvent {
    public float ScrollDelta { get; }

    public MouseScrollEvent(float scrollDelta) {
        ScrollDelta = scrollDelta;
    }
}