﻿using UnityEngine;

public class MouseButtonUpEvent : IEvent {
    public int Button { get; }
    public Vector3 MousePos { get; }

    public MouseButtonUpEvent(Vector3 mousePos, int button) {
        MousePos = mousePos;
        Button = button;
    }
}