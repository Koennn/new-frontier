﻿public class AbilityButtonPressedEvent : IEvent {
    public Actor Actor;
    public Ability Ability;

    public AbilityButtonPressedEvent(Actor actor, Ability ability) {
        Actor = actor;
        Ability = ability;
    }
}