public class UnitAbilityExecuteEvent : SyncEvent {
    public Ability Ability;
    public Actor Actor;
    public GridController Grid;

    public UnitAbilityExecuteEvent(bool fromLocal, Ability ability, Actor actor, GridController grid) : base(fromLocal) {
        Ability = ability;
        Actor = actor;
        Grid = grid;
    }
}