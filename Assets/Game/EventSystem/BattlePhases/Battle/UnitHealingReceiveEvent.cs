public class UnitHealingReceiveEvent : IEvent {
    public Actor Actor;
    public int Amount;

    public UnitHealingReceiveEvent(Actor actor, int amount) {
        Actor = actor;
        Amount = amount;
    }
}