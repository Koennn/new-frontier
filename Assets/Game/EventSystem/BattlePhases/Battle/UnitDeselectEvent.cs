public class UnitDeselectEvent : IEvent {
    public Actor Deselected;

    public UnitDeselectEvent(Actor deselected) {
        Deselected = deselected;
    }
}