public class BattlePhaseStartEvent : SyncEvent {
    public BattlePhaseStartEvent(bool fromLocal) : base(fromLocal) { }
}