public class UnitDamageReceiveEvent : IEvent {
    public Actor Actor;
    public Damage Damage;

    public UnitDamageReceiveEvent(Actor actor, Damage damage) {
        Actor = actor;
        Damage = damage;
    }
}