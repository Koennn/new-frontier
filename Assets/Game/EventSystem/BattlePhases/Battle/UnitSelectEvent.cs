public class UnitSelectEvent : IEvent {
    public Actor Selected;

    public UnitSelectEvent(Actor selected) {
        Selected = selected;
    }
}