public class AbilityCancelledEvent : IEvent {
    public Ability Ability;
    public GridController Grid;

    public AbilityCancelledEvent(Ability ability, GridController grid) {
        Ability = ability;
        Grid = grid;
    }
}