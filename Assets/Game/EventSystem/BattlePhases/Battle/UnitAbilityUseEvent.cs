public class UnitAbilityUseEvent : IEvent {
    public Actor Actor;
    public Ability Ability;

    public UnitAbilityUseEvent(Actor actor, Ability ability) {
        Actor = actor;
        Ability = ability;
    }
}