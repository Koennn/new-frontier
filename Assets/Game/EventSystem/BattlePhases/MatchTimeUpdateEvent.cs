public class MatchTimeUpdateEvent : IEvent {
    public int NewMatchTime;

    public MatchTimeUpdateEvent(int newMatchTime) {
        NewMatchTime = newMatchTime;
    }
}