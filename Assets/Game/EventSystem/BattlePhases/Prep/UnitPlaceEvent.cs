﻿using UnityEngine;

public class UnitPlaceEvent : IEvent {
    public string UnitName;
    public Vector2 Position;
    public bool Local;

    public UnitPlaceEvent(string unitName, Vector2 position, bool local) {
        UnitName = unitName;
        Position = position;
        Local = local;
    }
}