public class EndTurnEvent : SyncEvent {
    public Player LastTurn;
    public Player NextTurn;

    public EndTurnEvent(bool fromLocal, Player lastTurn, Player nextTurn) : base(fromLocal) {
        LastTurn = lastTurn;
        NextTurn = nextTurn;
    }
}