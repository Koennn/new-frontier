﻿public class StartUnitDragEvent : IEvent {
    public string UnitName;

    public StartUnitDragEvent(string unitName) {
        UnitName = unitName;
    }
}