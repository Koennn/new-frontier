public class PrepCompleteEvent : SyncEvent {
    public Player Player;

    public PrepCompleteEvent(bool fromLocal, Player player) : base(fromLocal) {
        Player = player;
    }
}