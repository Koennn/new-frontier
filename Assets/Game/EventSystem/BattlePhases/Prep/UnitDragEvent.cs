﻿using UnityEngine;

public class UnitDragEvent : IEvent {
    public string UnitName;
    public Vector2 MousePos;

    public UnitDragEvent(string unitName, Vector2 mousePos) {
        UnitName = unitName;
        MousePos = mousePos;
    }
}