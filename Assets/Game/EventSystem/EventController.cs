﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEngine;

public class EventController : MonoBehaviour {
    private readonly List<IEventListener> _eventListeners = new List<IEventListener>();
    public static EventController Instance;

    /// <summary>
    /// Initialize the EventController and register any events.
    /// </summary>
    private void Start() {
        Instance = this;
        _eventListeners.Add(new TestEventListener());
        _eventListeners.Add(new PrepPhaseListeners());
        _eventListeners.Add(new UiEventMappers());
        _eventListeners.Add(UIController.Instance);
        _eventListeners.Add(new BattlePhaseListeners());
        _eventListeners.Add(GameObject.Find("Control").GetComponent<Player>());
        _eventListeners.Add(GameObject.Find("AI").GetComponent<Player>());
        _eventListeners.Add(GameObject.Find("Control").GetComponent<TurnController>());
    }

    /// <summary>
    /// Executes the event methods of a certain type on all event listeners
    /// currently listening to this event.
    /// </summary>
    /// <param name="eventObject">Event type to call</param>
    public void CallEvent(IEvent eventObject) {
        foreach (var eventListener in _eventListeners) {
            if (eventListener == null) {
                continue;
            }
            foreach (var eventMethodInfo in GetMethodsWithAttribute(eventListener.GetType(), typeof(EventListener))) {
                var attributes = eventMethodInfo.GetCustomAttributes(typeof(EventListener), true);
                foreach (var attribute in attributes) {
                    if (attribute == null || attribute.GetType() != typeof(EventListener) || ((EventListener) attribute).EventType != eventObject.GetType()) {
                        continue;
                    }

                    eventMethodInfo.Invoke(eventListener, new object[] {eventObject});
                }
            }
        }
    }

    public void RegisterListener(IEventListener listener) {
        StartCoroutine(DelayedRegister(listener));
    }

    private IEnumerator DelayedRegister(IEventListener listener) {
        yield return new WaitForEndOfFrame();
        _eventListeners.Add(listener);
    }

    public void StartBattlePhase() {
        CallEvent(new BattlePhaseStartEvent(true));
    }

    /// <summary>
    /// Get all the methods in a class type with a certain attribute.
    /// </summary>
    /// <param name="classType">Class type to search in</param>
    /// <param name="attributeType">Attribute type to search for</param>
    /// <returns></returns>
    private static IEnumerable<MethodInfo> GetMethodsWithAttribute(Type classType, Type attributeType) {
        return classType.GetMethods().Where(methodInfo => methodInfo.GetCustomAttributes(attributeType, true).Length > 0);
    }
}