﻿using UnityEngine;

public class UnityKeyboardCaller : MonoBehaviour {
    private void FixedUpdate() {
        if (Input.anyKey) {
            EventController.Instance.CallEvent(new KeyPressEvent());
        }
    }
}