﻿using UnityEngine;
using UnityEngine.UI;

public class UnityButtonEventCallers : MonoBehaviour {
    private bool _isDragging;
    private string _dragUnit;
    private Button[] _unitButtons;

    public void OnUnitDragButtonPress(string unitName) {
        EventController.Instance.CallEvent(new StartUnitDragEvent(unitName));
        _isDragging = true;
        _dragUnit = unitName;
    }

    public void OnAbilityButtonPress() { }

    private void FixedUpdate() {
        if (!_isDragging) {
            return;
        }

        if (!Input.GetMouseButton(0)) {
            _isDragging = false;
            EventController.Instance.CallEvent(new StopUnitDragEvent());
            return;
        }
        EventController.Instance.CallEvent(new UnitDragEvent(_dragUnit, new Vector2(Input.mousePosition.x, Input.mousePosition.y)));
    }
}