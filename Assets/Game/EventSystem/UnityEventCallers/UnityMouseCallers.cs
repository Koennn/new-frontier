﻿using UnityEngine;

public class UnityMouseCallers : MonoBehaviour {
    private void FixedUpdate() {
        if (Input.GetMouseButtonUp(0)) {
            EventController.Instance.CallEvent(new MouseButtonUpEvent(Input.mousePosition, 0));
        }
        if (Input.GetMouseButtonUp(1)) {
            EventController.Instance.CallEvent(new MouseButtonUpEvent(Input.mousePosition, 1));
        }

        if (Input.GetMouseButtonDown(0)) {
            EventController.Instance.CallEvent(new MouseButtonDownEvent(Input.mousePosition, 0));
        }
        if (Input.GetMouseButtonDown(1)) {
            EventController.Instance.CallEvent(new MouseButtonDownEvent(Input.mousePosition, 1));
        }

        if (Input.mouseScrollDelta.y != 0) {
            EventController.Instance.CallEvent(new MouseScrollEvent(Input.mouseScrollDelta.y));
        }
    }
}