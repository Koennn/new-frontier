﻿using System;

public class EventReceiver {
    /// <summary>
    /// Create and call an event of a specified type with specified parameters.
    /// </summary>
    /// <param name="eventType">Type of the event</param>
    /// <param name="eventParams">Parameters of the event</param>
    public static void CreateAndCallEvent(Type eventType, object[] eventParams) {
        EventController.Instance.CallEvent((IEvent) Activator.CreateInstance(eventType, eventParams));
    }
}