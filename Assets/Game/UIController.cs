﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour, IEventListener {
    public static UIController Instance;

    public GameObject UnitPlace;
    public GameObject UnitAbilities;
    public GameObject AbilityHolder;
    public GameObject EndTurnButton;
    public Text MatchTime;

    private Actor _previousSelected;

    private void Start() {
        Instance = this;
    }

    [EventListener(typeof(UnitSelectEvent))]
    public void OnUnitSelect(UnitSelectEvent eventInfo) {
        if (_previousSelected != eventInfo.Selected && _previousSelected != null) {
            EventController.Instance.CallEvent(new UnitDeselectEvent(_previousSelected));
        }
        _previousSelected = eventInfo.Selected;

        UnitAbilities.SetActive(true);

        //TODO: Multiplayer compatible?
        var grid = GameObject.Find("Local Grid").GetComponent<GridController>();
        grid.ColorTile(grid.GetTileCoords(eventInfo.Selected.transform.parent.gameObject), new Color(64, 188, 193));

        var offset = 75;
        foreach (var ability in eventInfo.Selected.Abilities) {
            var abilityHolder = Instantiate(AbilityHolder, new Vector3(offset, 75, 0), Quaternion.identity, UnitAbilities.transform);
            if (abilityHolder == null) {
                continue;
            }

            var iconHolder = abilityHolder.transform.GetChild(0).gameObject.GetComponent<Image>();
            iconHolder.sprite = ability.Properties.Icon;

            var selectedAbility = ability;
            var button = abilityHolder.GetComponent<Button>();
            button.onClick = new Button.ButtonClickedEvent();
            button.onClick.AddListener(() => EventController.Instance.CallEvent(new AbilityButtonPressedEvent(eventInfo.Selected, selectedAbility)));
            offset += 160;
        }
    }

    [EventListener(typeof(UnitDeselectEvent))]
    public void OnUnitDeselect(UnitDeselectEvent eventInfo) {
        foreach (Transform child in UnitAbilities.transform) {
            Destroy(child.gameObject);
        }
        UnitAbilities.SetActive(false);

        //TODO: Multiplayer compatible?
        var grid = GameObject.Find("Local Grid").GetComponent<GridController>();
        Debug.Log("Deselecting: " + eventInfo.Deselected.Name);
        grid.ResetTileColor(grid.GetTileCoords(eventInfo.Deselected.transform.parent.gameObject));
    }

    [EventListener(typeof(BattlePhaseStartEvent))]
    public void OnBattlePhaseStart(BattlePhaseStartEvent eventInfo) {
        UnitPlace.SetActive(false);
        transform.Find("Button").gameObject.SetActive(false);
        TitleManager.Instance.DisplayTitle("The battle has started!", Color.black, 5);
    }

    [EventListener(typeof(MatchTimeUpdateEvent))]
    public void OnMatchTimeUpdate(MatchTimeUpdateEvent eventInfo) {
        var minutes = eventInfo.NewMatchTime / 60;
        var seconds = eventInfo.NewMatchTime % 60;
        MatchTime.text = "Match Time: " + minutes + ":" + (seconds < 10 ? "0" : "") + seconds;
    }

    [EventListener(typeof(EndTurnEvent))]
    public void OnEndTurn(EndTurnEvent eventInfo) {
        StartCoroutine(SetEndTurnButton(GameObject.Find("Control")));

        if (UnitAbilities.activeInHierarchy) {
            UnitAbilities.SetActive(false);
        }
    }

    private IEnumerator SetEndTurnButton(GameObject control) {
        yield return new WaitForFixedUpdate();
        EndTurnButton.SetActive(control.GetComponent<TurnController>().CurrentTurn == control.GetComponent<Player>());
    }
}